// [SECTION] Javascript Synchronous and Asynchronous
// Javascript is by default synchronous, it means that only one statement is excuted at a time.

// This can be proven when a statement has an error, javascript will not proceed with the next statement.
// console.log("Hello World");
// consloe.log("Hello World Again");
// console.log("Bye");

// Asynchronous means that we can proceed to excute other statements, while time consuming code is running in the background.

// for (i = 0; i <= 1000; i ++){
// 	console.log(i);
// }

// console.log("Hello Again");

// [Section] Getting all posts
// The Fetch API allows you to asynchronously request for a resources(data).
// A "promise" is an object that the eventual completion (or failure) of an asynchronous function and it's resulting value.
/*

	Syntax:
		fetch("URL");

*/

// A promise may be in one fo the 3 possible states: fullfiled, rejected, or pending.

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

/*
	Syntax:
		fetch("URL")
		.then((response) =>{//line code})

*/

// fetch("https://jsonplaceholder.typicode.com/posts")
// by using the response.status we only check if the promises is fulfilled or rejected.
// ".then" method captures the "response" object and returns another "promise" which will eventually "resolved" or "rejected".
// .then((response) => console.log(response.status))

// Use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application
// .then((response) => response.json())

// Using Multiple "then" methods creates a "promise chain".
// .then((json) => console.log(json));

// Display each title of the post
// .then((json) => {
// 	json.forEach(posts => console.log(posts.title));
// })

// The "async" and "await" keywords is another approach that can be used to achieve a asynchronous code.

async function fetchData(){

	// Await => Waits for the "fetch" method to complete then stores the value in the result variable.
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")

	console.log(result);

	// Converts the data from the "Response" object as JSON
	let json = await result.json()

	console.log(json);
}

fetchData();

// [SECTION] Getting a specific post
// (retrieve, /posts/:id, GET)
	// ":id" is a wildcard where you can put unique identifier as value to show a specific data/resources
// GET
fetch("https://jsonplaceholder.typicode.com/posts/45")
.then((response) => response.json())
.then((json) => console.log(json))

// [SECTION] Creating a post
/*
	Syntax:
		fetch("URL", options)
		.then((response) =>{})
		.then((json) => {})

*/
fetch("https://jsonplaceholder.typicode.com/posts", {
	
	// HTTP Method
	method: "POST",

	// Specify the content that it will be in JSON Format
	headers:{
		"Content-Type":"application/json"
	},

	// Sets the content/body data of the "Request" object to be sent to the backend/server.
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World!",
		userId: 1
	})

})
// response of the server base on the request
.then((response) => response.json())
.then((json) => console.log(json))

// [SECTION] Updating a post
// (update, /posts/:id, PUT)

// PUT Method is used to update the whole object
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title:"Updated post",
		body:"Hello again!",
		userId:1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// Patch Method is used to update a single/several properties of an object.
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title:"Corrected post"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// [SECTION] Deleting a post
// (delete, /posts/:id, DELETE)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})
