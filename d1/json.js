{
	"info": {
		"_postman_id": "047f00f4-d914-42ee-861a-f586ac27d0b2",
		"name": "s28",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json",
		"_exporter_id": "2229865"
	},
	"item": [
		{
			"name": "d1",
			"item": [
				{
					"name": "Get all posts",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "https://jsonplaceholder.typicode.com/posts",
							"protocol": "https",
							"host": [
								"jsonplaceholder",
								"typicode",
								"com"
							],
							"path": [
								"posts"
							]
						}
					},
					"response": []
				},
				{
					"name": "Get a single post",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "https://jsonplaceholder.typicode.com/posts/45",
							"protocol": "https",
							"host": [
								"jsonplaceholder",
								"typicode",
								"com"
							],
							"path": [
								"posts",
								"45"
							]
						}
					},
					"response": []
				},
				{
					"name": "Create a post",
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"title\":\"My First Blog Post\",\r\n    \"body\":\"Hello World\",\r\n    \"userId\": 1\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "https://jsonplaceholder.typicode.com/posts",
							"protocol": "https",
							"host": [
								"jsonplaceholder",
								"typicode",
								"com"
							],
							"path": [
								"posts"
							]
						}
					},
					"response": []
				},
				{
					"name": "Update a post",
					"request": {
						"method": "PUT",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"title\":\"Updated post\",\r\n    \"body\":\"Hello again!\",\r\n    \"userId\":1\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "https://jsonplaceholder.typicode.com/posts/1",
							"protocol": "https",
							"host": [
								"jsonplaceholder",
								"typicode",
								"com"
							],
							"path": [
								"posts",
								"1"
							]
						}
					},
					"response": []
				},
				{
					"name": "Update a specific part of the post",
					"request": {
						"method": "PATCH",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"title\":\"Corrected post\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "https://jsonplaceholder.typicode.com/posts/1",
							"protocol": "https",
							"host": [
								"jsonplaceholder",
								"typicode",
								"com"
							],
							"path": [
								"posts",
								"1"
							]
						}
					},
					"response": []
				},
				{
					"name": "Delete a post",
					"request": {
						"method": "DELETE",
						"header": [],
						"url": {
							"raw": "https://jsonplaceholder.typicode.com/posts/1",
							"protocol": "https",
							"host": [
								"jsonplaceholder",
								"typicode",
								"com"
							],
							"path": [
								"posts",
								"1"
							]
						}
					},
					"response": []
				}
			]
		}
	]
}
