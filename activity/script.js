{
	"info": {
		"_postman_id": "8217e3d2-bd51-4e79-9bd3-a445f146977c",
		"name": "s28",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json",
		"_exporter_id": "21501753"
	},
	"item": [
		{
			"name": "a1",
			"item": [
				{
					"name": "get all to do list items",
					"request": {
						"method": "GET",
						"header": []
					},
					"response": []
				},
				{
					"name": "get to do list item",
					"protocolProfileBehavior": {
						"disableBodyPruning": true
					},
					"request": {
						"method": "GET",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"title\":\"My First Blog Post\",\r\n    \"body\":\"Hello World\",\r\n    \"userId\": 1\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "https://jsonplaceholder.typicode.com/todos/1",
							"protocol": "https",
							"host": [
								"jsonplaceholder",
								"typicode",
								"com"
							],
							"path": [
								"todos",
								"1"
							]
						}
					},
					"response": []
				},
				{
					"name": "update to do list item",
					"request": {
						"method": "PUT",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \t\"dateCompleted\": \"Pending\",\r\n\t\t\"description\": \"to update my to do list with a different structure\",\r\n\t\t\"status\": \"Pending\",\r\n\t\t\"title\":\"Updated todo list item\",\r\n\t\t\"userId\":1\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "https://jsonplaceholder.typicode.com/todos/1",
							"protocol": "https",
							"host": [
								"jsonplaceholder",
								"typicode",
								"com"
							],
							"path": [
								"todos",
								"1"
							]
						}
					},
					"response": []
				},
				{
					"name": "create to do list items",
					"request": {
						"method": "PATCH",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n\t\t\"dateCompleted\":\"07/09/21\",\r\n\t\t\"status\": \"Complete\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "https://jsonplaceholder.typicode.com/posts/1",
							"protocol": "https",
							"host": [
								"jsonplaceholder",
								"typicode",
								"com"
							],
							"path": [
								"posts",
								"1"
							]
						}
					},
					"response": []
				},
				{
					"name": "DELETE a post",
					"request": {
						"method": "DELETE",
						"header": [],
						"url": {
							"raw": "https://jsonplaceholder.typicode.com/todos/1 ",
							"protocol": "https",
							"host": [
								"jsonplaceholder",
								"typicode",
								"com"
							],
							"path": [
								"todos",
								"1 "
							]
						}
					},
					"response": []
				},
				{
					"name": "create to do list item",
					"request": {
						"method": "PUT",
						"header": [],
						"url": {
							"raw": "https://jsonplaceholder.typicode.com/todos",
							"protocol": "https",
							"host": [
								"jsonplaceholder",
								"typicode",
								"com"
							],
							"path": [
								"todos"
							]
						}
					},
					"response": []
				}
			]
		}
	]
}